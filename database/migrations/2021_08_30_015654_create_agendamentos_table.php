<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedbigInteger('horario_id');
            $table->unsignedbigInteger('medico_id');
            $table->unsignedbigInteger('unidade_id');
            $table->unsignedbigInteger('especialidade_id');
            $table->unsignedbigInteger('user_id');


            $table->foreign('horario_id')
            ->references('id')->on('horarios')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('medico_id')
            ->references('id')->on('medicos')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('unidade_id')
            ->references('id')->on('unidades')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('especialidade_id')
            ->references('id')->on('especialidades')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->string('nome_paciente');
            $table->string('nome_medico');
            $table->string('nome_unidade');
            $table->string('nome_especialidade');
            $table->string('status');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamentos');
    }
}
