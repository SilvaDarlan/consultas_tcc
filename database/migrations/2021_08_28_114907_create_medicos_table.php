<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicos', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('crm');
            $table->string('cpf');
            $table->string('rg');
            $table->unsignedBigInteger('especialidade_id');
            $table->unsignedBigInteger('unidade_id');

            $table->foreign('especialidade_id')
            ->references('id')->on('especialidades')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('unidade_id')
            ->references('id')->on('unidades')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicos');
    }
}
