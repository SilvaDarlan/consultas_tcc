<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->id();
            $table->time('h_inicio');
            $table->time('h_final');
            $table->integer('dia_semana');
            $table->date('data');
            $table->integer('vagas');

            $table->unsignedBigInteger('especialidade_id')
            ->references('id')->on('especialidades')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedBigInteger('medico_id')
            ->references('id')->on('medicos')
            ->constrained()->onDelete('cascade')->onUpdate('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
