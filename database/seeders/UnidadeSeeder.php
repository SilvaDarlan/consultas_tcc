<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unidades = [
            ['nome' => 'UBS Antônio Simões', 'endereco' => 'R. Vitalino Stelim', 'numero' => '700', 'bairro' => 'CENTRO', 'cep' => '16700000', 'contato' => '1836061234', 'local' => 'https://www.google.com.br/maps/place/Centro+de+Sa%C3%BAde+(Post%C3%A3o)/@-21.2528464,-50.655775,14.25z/data=!4m9!1m2!2m1!1subs!3m5!1s0x0:0x6245a4d14d7a232e!8m2!3d-21.248476!4d-50.6431063!15sCgN1YnMiA4gBAZIBCGhvc3BpdGFs'],
            ['nome' => 'Santa Casa de Guararapes', 'endereco' => 'Av. Mal. Floriano', 'numero' => '1602', 'bairro' => 'CENTRO', 'cep' => '16700000', 'contato' => '1836061794', 'local' => 'https://www.google.com.br/maps/place/UBS+Ant%C3%B4nio+Sim%C3%B5es/@-21.2528464,-50.655775,14z/data=!4m9!1m2!2m1!1subs!3m5!1s0x9496590771b23cf3:0xeb6c7f21fc67ab0c!8m2!3d-21.248157!4d-50.659991!15sCgN1YnMiA4gBAZIBBmRvY3Rvcg'],
            ['nome' => 'Centro de Saúde (Postão)', 'endereco' => 'R. Maestro Pedro Sala', 'numero' => '920', 'bairro' => 'CENTRO', 'cep' => '16700000', 'contato' => '1834069010', 'local' => 'https://www.google.com.br/maps/place/Santa+Casa+de+Guararapes/@-21.251162,-50.6514814,17z/data=!4m5!3m4!1s0x0:0x2a94a807f05dfab0!8m2!3d-21.251167!4d-50.6492927']
        ];

        foreach ($unidades as $uni) {
            $this->insere_dados($uni);
        }
    }

    function insere_dados($uni){
        DB::table('unidades')->insert([
            'nome' => $uni['nome'],
            'endereco' => $uni['endereco'],
            'numero' => $uni['numero'],
            'bairro' => $uni['bairro'],
            'cep' => $uni['cep'],
            'contato' => $uni['contato'],
            'local' => $uni['local']
        ]);
    }
}
