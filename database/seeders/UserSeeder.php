<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['nome' => 'Administrador', 'email' => 'admin@email.com', 'password' => Hash::make('123456'), 'celular' => '(18) 98765-4321', 'endereco' => 'Rua 1', 'numero' => '1000', 'bairro' => 'Bairro 1', 'cep' => '10000-000', 'cpf' => '111.222.333-69', 'rg' => '00.111.222-3', 'admin' => 1],
            ['nome' => 'Paciente', 'email' => 'user@email.com', 'password' => Hash::make('123456'), 'celular' => '(18) 12345-6789', 'endereco' => 'Rua 2', 'numero' => '2000', 'bairro' => 'Bairro 2', 'cep' => '20000-000', 'cpf' => '999.666.555-14', 'rg' => '99.888.777-0', 'admin' => 0]
        ];

        foreach ($users as $user) {
            $this->insere_dados($user);
        }
    }

    function insere_dados($user){
        DB::table('users')->insert([
            'nome' => $user['nome'],
            'email' => $user['email'],
            'password' => $user['password'],
            'celular' => $user['celular'],
            'endereco' => $user['endereco'],
            'numero' => $user['numero'],
            'bairro' => $user['bairro'],
            'cep' => $user['cep'],
            'cpf' => $user['cpf'],
            'rg' => $user['rg'],
            'admin' => $user['admin']
        ]);
    }
}
