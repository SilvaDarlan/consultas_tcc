<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EspecialidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidades = [
            'Clínico Geral',
            'Pediatra',
            'Oftamologista',
            'Dermatologista',
            'Ginecologista',
            'Ortopedista',
            'Endocrinologista',
            'Pisicólogo',
            'Otorrinolaringologista'
        ];

        foreach ($especialidades as $esp) {
            $this->insere_dados($esp);
        }
    }

    function insere_dados($esp){
        DB::table('especialidades')->insert([
            'nome' => $esp
        ]);
    }
}
