<?php

namespace App\Http\Controllers;

use App\Models\Medico;

class MedicoEspecialidadeController extends Controller
{
    public function create() {
        $medico = Medico::all();
        return view('admin.medicos.list', compact('medicos'));
    }
}
