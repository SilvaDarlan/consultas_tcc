<?php

namespace App\Http\Controllers;

use App\Models\Agendamento;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function list() {
        $pacientes = User::where('admin', 0)->get();
        return view('admin.pacientes.list', compact('pacientes'));
    }

    public function create() {
        return view('admin.pacientes.create');
    }

    public function show($id) {
        $paciente = User::find($id);

        $dias = array('1' => 'Segunda', '2' => 'Terça', '3' => 'Quarta', '4' => 'Quinta', '5' => 'Sexta');

        $agendamentos = Agendamento::where('user_id', $id)
                                        ->where('status', 'Finalizado')
                                        ->get();

        foreach($agendamentos as $agendamento) {
            $agendamento->horario->dia_semana = $dias[ $agendamento->horario->dia_semana ];
        }

        $agendamentos2 = Agendamento::where('user_id', $id)
                                    ->where('status', '<>', 'Finalizado')
                                    ->orderBy('status')
                                    ->get();

        $classes = '';
        foreach($agendamentos2 as $agendamento2) {
            $agendamento2->horario->dia_semana = $dias[ $agendamento2->horario->dia_semana ];

        }

        return view('admin.pacientes.show', compact('paciente', 'agendamentos', 'agendamentos2'));
    }

    public function store(Request $request) {
        $paciente = User::create($request->all());
        return redirect()
                    ->route('paciente.show', $paciente->id)
                    ->with('mensagem', 'Paciente Cadastrado!');
    }

    public function update(Request $request, $id) 
    {
        if(!$paciente = User::find($id))
            return redirect()->back();

        $dados = $request->all();

        $paciente->update($dados);

        return redirect()
            ->route('pacientes.show', $paciente->id)
            ->with('mensagem', "Cadastro alterado com sucesso!");
    }
}
