<?php

namespace App\Http\Controllers;

use App\Models\Especialidade;
use Illuminate\Http\Request;

class EspecialidadeController extends Controller
{
    public function store(Request $request) {
        $especialidade = Especialidade::create($request->all());
        //
        echo json_encode($especialidade);
    }
}
