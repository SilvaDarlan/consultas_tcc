<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index() {
        return view('site.agendamentos.index');
    }

    public function register() {
        return view('site.register');
    }
}
