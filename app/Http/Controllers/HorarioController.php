<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHorario;
use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Medico;
use DateTime;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    public function index() {
        $horarios = Horario::orderBy('dia_semana', 'asc')
                                ->orderBy('data', 'desc')
                                ->get();
                                

        foreach($horarios as $horario) {
            if($horario->dia_semana == 1) {
                $horario->dia_semana = "Segunda";
            }
            elseif($horario->dia_semana == 2) {
                $horario->dia_semana = "Terça";
            }
            elseif($horario->dia_semana == 3) {
                $horario->dia_semana = "Quarta";
            }
            elseif($horario->dia_semana == 4) {
                $horario->dia_semana = "Quinta";
            }
            elseif($horario->dia_semana == 5) {
                $horario->dia_semana = "Sexta";
            }
        }

        return view('admin.horarios.list', compact('horarios'));
    }

    public function create() {
        $especialidades = Especialidade::all();
        $medicos = Medico::all();
        return view('admin.horarios.create', compact('especialidades', 'medicos'));
    }

    public function especialidades(Request $request) {
        $medico = Medico::where('id', $request->medico_id)->first();
        
        $response = '';
        foreach($medico->especialidades as $especialidade) {
            $response .= "<option value='{$especialidade->id}'>{$especialidade->nome}</option>";
        }
        echo json_encode($response);
        
    }

    public function store(StoreHorario $request) {
        //
        //gambiarra pra salvar os dias no banco
        //
        function dias($dias_semana, $mes, $ano) {
            $date = new DateTime();
            $dias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
            
            for ($dia = 0; $dia <= $dias; $dia++) {
                
                $date->setDate( $ano, $mes, $dia );

                    foreach ( $dias_semana as $_dia ) {
                        if ($date->format( "w" ) == $_dia)
                        {
                            (strlen($dia) == 2) ?: $dia = "0".$dia;
                            ($dia == 0) ?: $datas[$_dia][] = $ano."/".$mes."/".$dia;
                        }    
                    }
            }
            return $datas;
        }
        //
        $mes_ano = explode("-", $request['data']);

        $dias_semana = dias($request['dia_semana'], $mes_ano[1], $mes_ano[0]);

        foreach ($dias_semana as $semana => $dia_semana) {

            $request['dia_semana'] = $semana;

            foreach ($dia_semana as $dia) {
                $request['data'] = $dia;
                Horario::create($request->all());

            }

        }

        return redirect()
                    ->route('horarios.list')
                    ->with('mensagem', 'Horario Cadastrado!');
    }

    public function destroy($id) {
        $horario = Horario::find($id);

        $horario->delete();

        return redirect()
                    ->route('horarios.list')
                    ->with('mensagem', 'Horario apagado!');
    }

}

