<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMedico;
use App\Models\Agendamento;
use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Medico;
use App\Models\MedicoEspecialdiade;
use App\Models\Unidade;
use Illuminate\Http\Request;
use MedicoEspecialidade;

class MedicoController extends Controller
{
    public function index() {
        $medicos = Medico::all();
        return view('admin.medicos.list', compact('medicos'));
    }


    public function create() {
        $especialidades = Especialidade::all();
        $unidades = Unidade::all();
        return view('admin.medicos.create', compact('especialidades', 'unidades'));
    }

    public function editar($id) {
        $unidades = Unidade::all();
        $med_esps = MedicoEspecialdiade::where('medico_id', $id)->get();
        $especialidades = Especialidade::all();

        $medico = Medico::findOrFail($id);
        
        if(!$medico)
        return redirect()->back();

        return view('admin.medicos.edit', compact('medico', 'unidades', 'especialidades', 'med_esps'));
    }

    public function update(Request $request, $id){
        if(!$medico = Medico::find($id)) {
            return redirect()->back();
        }

        $med_esps = MedicoEspecialdiade::where('medico_id', $id)->get();
        if(!empty($request->especialidade_ids)) {
            foreach($request->especialidade_ids as $esp) {
                MedicoEspecialdiade::create([
                    'medico_id' => $id,
                    'especialidade_id' => $esp
                ]);
            }
        }
        $dados = $request->all();

        $medico->update($dados);
        
        return redirect()
            ->route('medicos.edit', $medico->id)
            ->with('mensagem', "Registro alterado com sucesso!");
    }
    
    public function store(StoreMedico $request) {
        $medico = Medico::create($request->all());
        $request['medico_id'] = $medico->id;
        MedicoEspecialdiade::create($request->all('medico_id', 'especialidade_id'));
        
        return redirect()
                    ->route('medicos.list')
                    ->with('mensagem', "Dr(a). {$medico->nome} Cadastrado(a)!")
                    ->with('id', $medico->id);
    }

    public function show($id) {
        $medico = Medico::findOrFail($id);
        return view('admin.medicos.show', compact('medico'));
    }

    public function destroy($id) {
        $medico = Medico::find($id);

        $medico->delete();

        return redirect()
                    ->route('medicos.list')
                    ->with('mensagem', 'Registro de Médico apagado!');
    }
}
