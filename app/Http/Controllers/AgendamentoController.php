<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAgendamentos;
use App\Models\Agendamento;
use App\Models\Especialidade;
use App\Models\Horario;
use App\Models\Medico;
use App\Models\Unidade;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nexmo\Laravel\Facade\Nexmo;

class AgendamentoController extends Controller
{
    function __construct(){
        $this->tamanhoPaginacao = 5;
    }
    public function index() {
        if(Auth::user()->admin == 1) {
            $agendamentos = Agendamento::orderBy('status', 'asc')
                                        ->latest()->paginate($this->tamanhoPaginacao);
        }
        else {
            $agendamentos = Agendamento::where('user_id', Auth::user()->id)
                                            ->where('status', '<>', 'Finalizado')
                                            ->get();
        }

        $dias = array('1' => 'Segunda', '2' => 'Terça', '3' => 'Quarta', '4' => 'Quinta', '5' => 'Sexta');

        foreach($agendamentos as $agendamento) {
            $agendamento->horario->dia_semana = $dias[ $agendamento->horario->dia_semana ];
        }

        return view('site.agendamentos.index', compact('agendamentos'));
    }

    public function historico() {
        $agendamentos = Agendamento::where('user_id', Auth::user()->id)
                                        ->where('status', 'Finalizado')->get();

        $dias = array('1' => 'Segunda', '2' => 'Terça', '3' => 'Quarta', '4' => 'Quinta', '5' => 'Sexta');

        foreach($agendamentos as $agendamento) {
            $agendamento->horario->dia_semana = $dias[ $agendamento->horario->dia_semana ];
        }

        return view('site.agendamentos.hist', compact('agendamentos'));
    }

    public function create() {
        $especialidades = Especialidade::get();
        $unidades = Unidade::get();
        $medicos = Medico::get();
        $horarios = Horario::get();
        $pacientes = '';
        if(Auth::user()->admin == 1) {
            $pacientes = User::where('admin', 0)->get();
        }

        return view('site.agendamentos.create', compact('horarios', 'especialidades', 'medicos', 'unidades', 'pacientes'));
    }

    public function store(Request $request) {
        $horario = Horario::find($request->horario_id);
        if(Auth::user()->admin == 1) {
            $paciente = User::find($request->user_id);
            $status = $request->status;
        }
        else {
            $paciente = Auth::user();
            $status = 'Solicitado';
        }
        Agendamento::create(
            [
                'horario_id' => $horario->id,
                'especialidade_id' => $horario->especialidade->id,
                'nome_especialidade' => $horario->especialidade->nome,
                'medico_id' => $horario->medico->id,
                'nome_medico' => $horario->medico->nome,
                'unidade_id' => $horario->medico->unidade->id,
                'nome_unidade' => $horario->medico->unidade->nome,
                'user_id' => $paciente->id,
                'nome_paciente' => $paciente->nome,
                'status' => $status
            ]
        );
        //
        $horario->vagas = $horario->vagas_restantes;
        $horario->save();
        //

        return redirect()
                    ->route('agendamentos.index')
                    ->with('mensagem', 'Agendamento ' . $status . '!');
    }

    public function show($id) {
        $agendamento = Agendamento::findOrFail($id);

        return view('site.agendamentos.show', compact('agendamento'));
    }

    public function datas(Request $request) {
        $horarios = Horario::where('especialidade_id', $request->especialidade)
                            ->where('vagas', '>', 0)
                        ->get();
        foreach ($horarios as $horario) {
            $datas[] = [
                'start' => $horario->data->format('Y-m-d'),
                'end' => $horario->data->format('Y-m-d'),
                'overlap' => false,
                'rendering' => 'background'
            ];
        }
        if(!empty($datas)) {   
            echo json_encode($datas); 
        }
        else {
            echo json_encode("Nenhum horário encontrado.");
        }
    }

    public function horarios(Request $request) {
        $horarios = Horario::where('data', $request->data)
                            ->where('especialidade_id', $request->especialidade)
                        ->get();
        if(count($horarios) > 0) {
            $rads_horario = "
                <table class='table'>
                    <thead>
                        <tr class='table-dark'>
                            <th scope='col'>Médico</th>
                            <th scope='col'>Especialidade</th>
                            <th scope='col'>Unidade de Atendimento</th>
                            <th scope='col'>Vagas Restantes</th>
                            <th scope='col' class='text-center'>Selecionar</th>
                        </tr>
                    </thead>
                <tbody>";
    
            foreach($horarios as $horario) {
                $rads_horario .= "
                        <tr>
                            <th scope='row'>" . $horario->medico->nome . "</th>
                            <td>" . $horario->especialidade->nome . "</td>
                            <td>" . $horario->medico->unidade->nome . "</td>
                            <td>" . $horario->vagas . "</td>
                            <td class='text-center'>
                                <input type='radio' class='btn-check p-0 m-0' name='horario_id' id='primary-outlined_" . $horario->id ."' autocomplete='off' value='" . $horario->id . "'>
                                <label class='btn btn-outline-primary p-0 px-1 m-0 text-white' for='primary-outlined_" . $horario->id ."'><i class='bi bi-check'></i></label>
                            </td>
                        </tr>";
            }
    
            $rads_horario .= "
                </tbody>
            </table>";
            echo json_encode($rads_horario); 
        }
        else {
            echo json_encode("Nenhum horário encontrado!"); 
        }
    }

    public function update(Request $request, $id)
    {
        $agendamento = Agendamento::find($id);
        if($request->status == "Confirmar") {
            $request->status = "Confirmado";
        }
        
        if($request->status == "Recusar") {
            $request->status = "Recusado";
        }
        if($request->status == "Finalizar") {
            $request->status = "Finalizado";
        }
        
        
        $message = "Oi {$agendamento->user->nome}, o agendamento da sua consulta de " . $agendamento->especialidade->nome .  " no site AgendaPlus entre " . $agendamento->horario->h_inicio->format('H:i') . " e " . $agendamento->horario->h_final->format('H:i') . " do dia " . $agendamento->horario->data->format('d/m/Y') . " foi " . strtoupper($request->status) . "!";
        if($request->motivo != '') {
            $message .= " Motivo: {$request->motivo}";
        }
        Nexmo::message()->send([
            'to' => '+5518981487723',
            'from' => '+5518981487723',
            'text' => $message
        ]);
        $agendamento->status = $request->status;
        $agendamento->save();
        return redirect()
        ->route('agendamentos.index')
        ->with('mensagem', 'O agendamento foi ' . $agendamento->status . '!');
    }

    public function getPaciente(Request $request) {
        $paciente = User::find($request->_user_id);

        echo json_encode($paciente->nome . " | " . $paciente->cpf . " | " . $paciente->celular);
    }

    public function pesquisar(Request $request){

        $agendamentos = Agendamento::where(function ($query) use ($request) {
                                        $query->where("nome_paciente", 'LIKE', "%{$request->pesquisa}%")
                                                ->orWhere('nome_medico', 'LIKE', "%{$request->pesquisa}%")
                                                ->orWhere('nome_unidade', 'LIKE', "%{$request->pesquisa}%")
                                                ->orWhere('nome_especialidade', 'LIKE', "%{$request->pesquisa}%");
                                    })
                                    ->where(function ($query) use ($request) {
                                        if($request->status != 'Todos') {
                                            $query->where('status', $request->status);
                                        }
                                    })
                                    ->orderBy('status', 'asc')
                                    ->get();
        $retorno = "";
        $status = '';

        $dias = array('1' => 'Segunda', '2' => 'Terça', '3' => 'Quarta', '4' => 'Quinta', '5' => 'Sexta');
        foreach ($agendamentos as $agendamento) {
        $agendamento->horario->dia_semana = $dias[ $agendamento->horario->dia_semana ];
        if ($agendamento->status != $status) {
            $retorno .= "<h3 class='text-muted'>" . $status = ($agendamento->status);
            $retorno .= "</h3>";
        }
          $retorno .= "
            <div class='agendamento row d-flex justify-content-center rounded-3 p-3'>
            <div class='col-2 d-flex align-content-center flex-wrap justify-content-center'>";
              if ($agendamento->status == 'Solicitado') {
                $retorno .= "<i class='bi bi-clock display-1 text-warning' title='Solicitado'></i>";

              }
              if ($agendamento->status == 'Confirmado') {

                $retorno .= "<i class='bi bi-calendar-check display-1 text-success text-dark' title='Confirmado'></i>";
            }
            if ($agendamento->status == 'Finalizado') {
                $retorno .= "<i class='bi bi-calendar-check-fill text-success display-1' title='Finalizado'></i>";

            }
              if ($agendamento->status == 'Recusado') {

                $retorno .= "<i class='bi bi-calendar-x-fill text-danger display-1' title='Recusado'></i>";
            }

            $retorno .= "</div>
            <div class='col-4'>
              <p class=''><b>" . $agendamento->nome_especialidade . "</b></p>
              
              <p class='text-muted p-0 m-0'> " . $agendamento->nome_medico . "</p>
              <p class='text-muted p-0 m-0'>" .
                 $agendamento->horario->dia_semana . ", " . $agendamento->horario->data->format('d/m/Y') . "
              </p>
              <p class='text-muted p-0 m-0'>
                Status: " . $agendamento->status . "
              </p>";
              if (Auth::user()->admin == 1) {
                  $retorno .= "<p class='text-muted p-0 m-0'> Paciente: " . $agendamento->nome_paciente . " </p>";
                }
              $retorno .= "<a href='". $agendamento->unidade->local . "' target='_blank' class='text-decoration-none text-muted local' title='Ver " . $agendamento->nome_unidade ." no mapa'>
                <i class='bi bi-geo-alt-fill'></i><b> " . $agendamento->nome_unidade . " </b>
    
              </a>
            </div>
            <div class='col-2 d-flex align-content-center flex-wrap justify-content-center'>
              <a href='" . route('agendamentos.show', $agendamento->id) . "' class='text-decoration-none' style='color: #000;' title='Ver Agendamento'>
                <i class='bi bi-arrow-right display-5 p-3 ver'></i>
              </a>
            </div>
            <div class='col-1 d-flex align-content-center flex-wrap justify-content-center'>";
    
            if (Auth::user()->admin == 1) {
                if($agendamento->status == 'Solicitado') {
                    $retorno .= "<div class='row'>
                      <div class='col d-flex'>
                        <form action='" .  route('agendamentos.update', $agendamento->id) . "' method='POST'>
                        <input type='hidden' name='_token' value='" . csrf_token() . "'>
                        <input type='hidden' name='_method' value='put'>
                          <input class='btn btn-success btn-sm m-1' type='submit' name='status' id='btnStatus' value='Confirmar'>
                        </form>
          
                        <form action='" . route('agendamentos.update', $agendamento->id) . "' method='POST'>
                        <input type='hidden' name='_token' value='" . csrf_token() . "'>
                        <input type='hidden' name='_method' value='put'>
                        <input class='btn btn-danger btn-sm m-1' type='submit' name='status' id='btnStatus' value='Recusar'>
                        </form>
                      </div>
                    </div>";

                }

                if($agendamento->status == 'Confirmado') {
                    $retorno .= "<div class='row'>
                      <div class='col d-flex'>
                        <form action='" . route('agendamentos.update', $agendamento->id) . "' method='POST'>
                        <input type='hidden' name='_token' value='" . csrf_token() . "'>
                        <input type='hidden' name='_method' value='put'>
                          <input class='btn btn-success btn-sm m-1' type='submit' name='status' id='btnStatus' value='Finalizar'>
                        </form>
                      </div>
                    </div>";

                }

            }
                
            
                
            $retorno .= "</div>
          </div>
          <hr class='m-1'>";
          
        }
        echo json_encode($retorno);
    }
}