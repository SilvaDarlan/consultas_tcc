<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'telefone' => 'nullable',
            'celular' => 'required',
            'endereco' => 'required|string',
            'numero' => 'required',
            'bairro' => 'required|string',
            'cep' => 'required',
            'cpf' => 'required',
            'rg' => 'required',

            'password' => 'required'

        ]);
        
        $data = $request->all();
        $data['password'] = Hash::make($request->password);

        $user = User::create($data);
        event(new Registered($user));

        if(Auth::guest()) {
            Auth::login($user);

            return redirect()
            ->route('agendamentos.index')
            ->with('mensagem', 'Cadastro realizado. Seja bem Vindo!');
        }
        if(Auth::user()->admin == 1) {
            return redirect()
            ->route('pacientes.list')
            ->with('mensagem', 'Cadastro realizado com Sucesso!')
            ->with('id', $user->id);
        }
            
    }
}
