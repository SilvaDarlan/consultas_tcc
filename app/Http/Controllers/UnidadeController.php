<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUnidade;
use App\Models\Unidade;
use Illuminate\Http\Request;

class UnidadeController extends Controller
{
    public function index() {
        $unidades = Unidade::all();
        return view('admin.unidades.list', compact('unidades'));
    }

    public function create() {
        return view('admin.unidades.create');
    }

    public function store(StoreUnidade $request) {
        $unidade = Unidade::create($request->all());
        return redirect()
                    ->route('unidades.list')
                    ->with('mensagem', "Unidade {$unidade->nome} Cadastrada!")
                    ->with('id', $unidade->id);
    }

    public function show($id) {
        $unidade = Unidade::findOrFail($id);

        return view('admin.unidades.show', compact('unidade'));
    }
}
