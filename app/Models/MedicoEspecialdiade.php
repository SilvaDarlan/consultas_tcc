<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicoEspecialdiade extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'medico_especialidade';
    protected $fillable = [
        'medico_id',
        'especialidade_id'
    ];
}
