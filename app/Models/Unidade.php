<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidade extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'unidades';
    protected $fillable = [
        'nome',
        'endereco',
        'bairro',
        'numero',
        'cep',
        'local',
        'contato'
    ];

    public function medicos() {
        return $this->hasMany(Medico::class);
    }

    public function agendamentos() {
        return $this->hasMany(Agendamento::class);
    }

}
