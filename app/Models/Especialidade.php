<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especialidade extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'especialidades';
    protected $fillable = ['nome'];

    public function medicos() {
        return $this->belongsToMany(Medico::class, 'medico_especialidade');
    }

    public function horarios() {
        return $this->hasMany(Horario::class);
    }

    public function agendamentos() {
        return $this->hasMany(Agendamento::class);
    }
}

