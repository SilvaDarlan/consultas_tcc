<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Horario extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'horarios';
    protected $fillable = [
        'dia_semana',
        'h_inicio',
        'h_final',
        'vagas',
        'especialidade_id',
        'medico_id',
        'data'
    ];

    protected $dates = ['data', 'h_inicio', 'h_final'];

    public function especialidade() {
        return $this->belongsTo(Especialidade::class);
    }

    public function medico() {
        return $this->belongsTo(Medico::class);
    }

    public function agendamentos() {
        return $this->hasMany(Agendamento::class);
    }

    function getVagasRestantesAttribute(){
        $this->loadCount('agendamentos');
        return $this->vagas - 1;
    }
}

