<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medico extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'medicos';
    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'crm',
        'cpf',
        'rg',
        'especialidade_id',
        'unidade_id'
    ];

    public function especialidades() {
        return $this->belongsToMany(Especialidade::class, 'medico_especialidade');
    }

    public function unidade() {
        return $this->belongsTo(Unidade::class);
    }

    public function horarios() {
        return $this->hasMany(Horario::class);
    }

    public function agendamentos() {
        return $this->hasMany(Agendamento::class);
    }
}
