<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agendamento extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'agendamentos';
    protected $fillable = [
        'horario_id',
        'especialidade_id',
        'medico_id',
        'unidade_id',
        'user_id',
        'status',
        'nome_paciente',
        'nome_medico',
        'nome_unidade',
        'nome_especialidade'
    ];

    public function horario() {
        return $this->belongsTo(Horario::class);
    }

    public function medico() {
        return $this->belongsTo(Medico::class);
    }

    public function unidade() {
        return $this->belongsTo(Unidade::class);
    }

    public function especialidade() {
        return $this->belongsTo(Especialidade::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }


}
