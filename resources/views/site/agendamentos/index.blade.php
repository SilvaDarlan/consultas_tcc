@extends('admin.layout.app')

@section('title', 'Agendamentos')

<style>
	.agendamento:hover {
		background: rgba(0, 0, 0, 0.1);

	}
	.agendamento {
		transition: background .5s;
		cursor: pointer;
	}
	/*******/
	.agendar {
		transition: font-size .3s;
	}
	.agendar:hover {
		text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
		font-size: 30px;
	}
	/*******/
	.ver {
		transition: font-size .2s;
	}
	.ver:hover {
		font-size: 60px;
	}
	/*******/
	.local {
		transition: font-size .1s;
	}
	.local:hover {
		font-size: 18px;
	}


	#btnStatus {
		display: none;
	}

	.agendamento:hover #btnStatus {
		display: inline-block;
	}

</style>

@section('content')
    <h2 class="display-5 text-center" style="font-weight: 400">Consultas Agendadas</h2>
    <hr>
    <div class="text-center">
      <a style="text-decoration: none; color: #000;" href="{{ route('agendamentos.create') }}" class="agendar h4 p-3 text-muted" title="Agendar Nova Consulta"> <i class="bi bi-plus-lg"></i> Agendar Consulta</a>
    </div>
    <hr>
    @if($mensagem = Session::get('mensagem'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{ $mensagem }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
    </div>
    @endif
    @php
        $status = "";
    @endphp
    @if (Auth::user()->admin == 1)
        
    <div class="row">
      <div class="row text-center justify-content-center">
        <div class="col-6">
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status" id="radio_status_todos" value="Todos" checked>
            <label class="form-check-label" for="radio_status_todos">Todos</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status" id="radio_status_solicitados" value="Solicitado">
            <label class="form-check-label" for="radio_status_solicitados">Solicitados</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status" id="radio_status_confirmados" value="Confirmado">
            <label class="form-check-label" for="radio_status_confirmados">Confirmados</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status" id="radio_status_recusados" value="Recusado">
            <label class="form-check-label" for="radio_status_recusados">Recusados</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" name="status" id="radio_status_finalizados" value="Finalizado">
            <label class="form-check-label" for="radio_status_finalizados">Finalizados</label>
          </div>

        </div>

      </div>
      <div class="row mt-3 mb-3 justify-content-center">
        <div class="col-4">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Paciente, Médico, Unidade ..." id="consulta">
            <button class="btn btn-outline-primary" type="button" id="btnSearch" title="Pesquisar" value="{{ old('pesquisa') }}"><i class="bi bi-search"></i></button>
          </div>

        </div>

      </div>
    </div>
    <hr>
    @endif
    <div class="principal_agendamentos m-0 p-0">
      @foreach ($agendamentos as $agendamento)
      @if ($agendamento->status != $status)
          <h3 class="text-muted">{{ $status = $agendamento->status }}</h3>
      @endif
        <div class="agendamento row d-flex justify-content-center rounded-3 p-3">
          <div class="col-2 d-flex align-content-center flex-wrap justify-content-center">
            @if ($agendamento->status == 'Solicitado')
              <i class="bi bi-clock display-1 text-warning" title="Solicitado"></i>
            @endif
            @if ($agendamento->status == 'Confirmado')
             <i class="bi bi-calendar-check display-1 text-success text-dark" title="Confirmado"></i>
            @endif
            @if ($agendamento->status == 'Finalizado')
            <i class="bi bi-calendar-check-fill text-success display-1" title="Finalizado"></i>
            @endif
            @if ($agendamento->status == 'Recusado')
            <i class="bi bi-calendar-x-fill text-danger display-1" title="Recusado"></i>
            @endif
          </div>
          <div class="col-4">
            <p class=""><b>{{ $agendamento->especialidade->nome }}</b></p>
            <p class="text-muted p-0 m-0"> {{ $agendamento->medico->nome }} </p>
            <p class="text-muted p-0 m-0">
              {{ $agendamento->horario->dia_semana }}, {{ $agendamento->horario->data->format('d/m/Y') }}
            </p>
            <p class="text-muted p-0 m-0">
              Status: {{ $agendamento->status }}
            </p>
            @if (Auth::user()->admin == 1)
              <p class="text-muted p-0 m-0"> Paciente: {{ $agendamento->user->nome }} </p>
            @endif
            <a href="{{ $agendamento->unidade->local }}" target="_blank" class="text-decoration-none text-muted local" title="Ver {{ $agendamento->unidade->nome }} no mapa">
              <i class="bi bi-geo-alt-fill"></i><b> {{ $agendamento->unidade->nome }} </b>
  
            </a>
          </div>
          <div class="col-2 d-flex align-content-center flex-wrap justify-content-center">
            <a href="{{route('agendamentos.show', $agendamento->id)}}" class="text-decoration-none" style="color: #000;" title="Ver Agendamento">
              <i class="bi bi-arrow-right display-5 p-3 ver"></i>
            </a>
          </div>
          <div class="col-1 d-flex align-content-center flex-wrap justify-content-center">
  
            @if (Auth::user()->admin == 1)
              @if($agendamento->status == 'Solicitado')
              <div class="row">
                <div class="col d-flex">
                  <form action="{{ route('agendamentos.update', $agendamento->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <input class="btn btn-success btn-sm m-1" type="submit" name="status" id="btnStatus" value="Confirmar">
                  </form>
    
                  <form id="form_recusar" action="{{ route('agendamentos.update', $agendamento->id) }}" method="POST">
                    @csrf
                    @method('put')
				  <button class="btn btn-danger btn-sm m-1" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" id="btnStatus">Recusar</button>
				  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Deseja realmente recusar esse agendamento?</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						  </div>
						  <div class="modal-body">
							<div class="mb-3">
								<label for="exampleFormControlInput1" class="form-label">Motivo:</label>
								<input type="text" class="form-control" id="motivo" name="motivo">
							  </div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
							<input type="hidden" name="status" value="Recusar">
							<button type="button" class="btn btn-primary" id="btnRecusar">Prosseguir</button>
						  </div>
						</div>
					  </div>
					</div>
                  </form>
                </div>
              </div>
              @endif
              @if($agendamento->status == 'Confirmado')
              <div class="row">
                <div class="col d-flex">
                  <form action="{{ route('agendamentos.update', $agendamento->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <input class="btn btn-success btn-sm m-1" type="submit" name="status" id="btnStatus" value="Finalizar">
                  </form>
                </div>
              </div>
              @endif
              
            @endif
              
          </div>
        </div>

        <hr class="m-1">
      @endforeach

    </div>
    <div class="col-12">
      @if (Auth::user()->admin == 1)
      <div class="justify-content-center d-flex">
          {{ $agendamentos->links() }}
      </div>
      @endif
  </div>
@endsection
@section('body')

    <script>
		$("#consulta").on('keyup', function() {
				var _status = $("input[name='status']:checked").val();
			$.post("{{ route('agendamentos.search') }}",
				{
					pesquisa: $('#consulta').val(),
					status: _status
				},
				function (data) {
					console.log(data)
					if(data == '') {
						$(".principal_agendamentos").html('<h5>Nenhum agendamento encontrado</h5>');
					}
					else {
						$(".principal_agendamentos").html(data);
					}
				},
				"json"
			);
		});

		$("#btnSearch").click( function() {
			$(".principal_agendamentos").html("<div class='text-center mb-3'><div class='spinner-border  text-primary' role='status' style='width: 5rem; height: 5rem;'><span class='visually-hidden'>Carregando...</span></div></div>");
					var _status = $("input[name='status']:checked").val();
			$.post("{{ route('agendamentos.search') }}",
				{
					pesquisa: $('#consulta').val(),
					status: _status
				},
				function (data) {
					console.log(data)
					setTimeout(() => {
						if(data == '') {
							$(".principal_agendamentos").html("<h4 class='text-muted text-center'>Nenhum agendamento encontrado</h4>");
						}
						else {
			
							$(".principal_agendamentos").html(data);
						}
					}, 500);
				},
				"json"
			);
		});

		$("#btnRecusar").click(function() {
			if($("#motivo").val() == '') {
				$("#motivo").focus();
				return;
			}

			$("#form_recusar").submit();
		});
    </script>
@endsection

