@extends('admin.layout.app')

@section('title', 'Histórico de Consultas')

<style>
	.agendamento:hover {
		background: rgba(0, 0, 0, 0.1);

	}
	.agendamento {
		transition: background .5s;
		cursor: pointer;
	}
	/*******/
	.agendar {
		transition: font-size .3s;
	}
	.agendar:hover {
		text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
		font-size: 30px;
	}
	/*******/
	.ver {
		transition: font-size .2s;
	}
	.ver:hover {
		font-size: 60px;
	}
	/*******/
	.local {
		transition: font-size .1s;
	}
	.local:hover {
		font-size: 18px;
	}


	#btnStatus {
		display: none;
	}

	.agendamento:hover #btnStatus {
		display: inline-block;
	}

</style>

@section('content')
    <h2 class="display-5 text-center" style="font-weight: 400">Histórico de Consultas</h2>
    <hr>
    <div class="text-center">
      <a style="text-decoration: none; color: #000;" href="{{ route('agendamentos.create') }}" class="agendar h4 p-3 text-muted" title="Agendar Nova Consulta"> <i class="bi bi-plus-lg"></i> Agendar Consulta</a>
    </div>
    <hr>
    @if($mensagem = Session::get('mensagem'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{ $mensagem }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
    </div>
    @endif
    <div class="principal_agendamentos m-0 p-0">
		@if (count($agendamentos) > 0)
			@foreach ($agendamentos as $agendamento)
				<h3 class="text-muted">{{ $agendamento->status }}</h3>
			<div class="agendamento row d-flex justify-content-center rounded-3 p-3">
				<div class="col-2 d-flex align-content-center flex-wrap justify-content-center">
					<i class="bi bi-calendar-check-fill text-success display-1" title="Finalizado"></i>
				</div>
				<div class="col-4">
					<p class=""><b>{{ $agendamento->especialidade->nome }}</b></p>
					<p class="text-muted p-0 m-0"> {{ $agendamento->medico->nome }} </p>
					<p class="text-muted p-0 m-0">
						{{ $agendamento->horario->dia_semana }}, {{ $agendamento->horario->data->format('d/m/Y') }}
					</p>
					<p class="text-muted p-0 m-0">
						Status: {{ $agendamento->status }}
					</p>
					<a href="{{ $agendamento->unidade->local }}" target="_blank" class="text-decoration-none text-muted local" title="Ver {{ $agendamento->unidade->nome }} no mapa">
						<i class="bi bi-geo-alt-fill"></i><b> {{ $agendamento->unidade->nome }} </b>
			
					</a>
				</div>
				<div class="col-2 d-flex align-content-center flex-wrap justify-content-center">
				<a href="{{route('agendamentos.show', $agendamento->id)}}" class="text-decoration-none" style="color: #000;" title="Ver Agendamento">
					<i class="bi bi-arrow-right display-5 p-3 ver"></i>
				</a>
				</div>
			</div>
	
			<hr class="m-1">
			@endforeach
		@else
			<h4 class="text-center text-muted">Nenhuma Consulta Encontrada.</h4>
		@endif
    </div>
@endsection


