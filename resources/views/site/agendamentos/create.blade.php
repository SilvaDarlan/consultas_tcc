@extends('admin.layout.app')

@section('title', 'Agendar Consulta')

@section('head')
    <meta charset='utf-8' />
    <link href="{{asset('assets/fullcalendar/packages/core/main.css')}}" rel='stylesheet' />
    <link href="{{asset('assets/fullcalendar/packages/daygrid/main.css')}}" rel='stylesheet' />
    <link href="{{asset('assets/fullcalendar/packages/timegrid/main.css')}}" rel='stylesheet' />
    <link href="{{asset('assets/fullcalendar/packages/list/main.css')}}" rel='stylesheet' />
    <link href="{{asset('assets/fullcalendar/css/style.css')}}" rel='stylesheet' />
    <script src="{{asset('assets/fullcalendar/packages/core/main.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/packages/interaction/main.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/packages/daygrid/main.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/packages/timegrid/main.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/packages/list/main.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/packages/core/locales-all.js')}}"></script>
    <script src="{{asset('assets/fullcalendar/js/calendar.js')}}"></script>
	<script>
		// document.addEventListener('DOMContentLoaded', function() {

		// });

	</script>
	<style>
		.fc-view {
			cursor: pointer;
		}
		
	</style>
    
@endsection

@section('content')
	<h2 class="display-5 text-center">Agendar nova Consulta</h2>
	<hr>
	@if ($errors->any())
		<div class="alert alert-danger">
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			<strong>Ops!</strong> Não foi possível agendar essa consulta.
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		</div>
	@endif
		<div class="modal fade" tabindex="-1" id="modal-data">
			<div class="modal-dialog modal-lg">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title">Agendar Consulta</h5>
				  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<form action="{{ route('agendamentos.store') }}" method="POST" id="form_agendar">
					@csrf
					<div class="modal-body" id="modal-body">
						
						@if (Auth::user()->admin == 1)
						<input type="hidden" name="user_id" id="user_id" value="@if(isset($_GET['id'])){{$_GET['id']}} @endif">
							<label for="status" class="form-label">Status</label>
								<select class="form-select" name="status">
									<option value="Confirmado" selected>Confirmar</option>
									<option value="Solicitado">Solicitar</option>
								</select>
						@endif
						<div class="form-group" id="div_horarios">

						</div>
					</div>
					<div class="modal-footer">
				  		<button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
				  		<button type="button" class="btn btn-dark" onclick="confirmar()">Confirmar</button>
					</div>
				</form>				  
			  </div>
			</div>
		  </div>
		  @if (Auth::user()->admin == 1)
		  <div class="modal fade" tabindex="-1" id="modal-pacientes">
			<div class="modal-dialog modal-xl">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title">Pacientes</h5>
				  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
					<div class="modal-body" id="modal-body">
						<div class="pacientes">
							<table class="table">
								<thead>
								  <tr class="table-dark">
									<th scope="col">#</th>
									<th scope="col">Nome</th>
									<th scope="col">Endereco</th>
									<th scope="col">Celular</th>
									<th scope="col">CPF</th>
									<th scope="col" class="text-center">Selecionar</th>
								  </tr>
								</thead>
								<tbody>
									@foreach ($pacientes as $paciente)
										<tr>
											<th scope="row">{{ $paciente->id }}</th>
											<td>{{ $paciente->nome }}</td>
											<td>{{ $paciente->endereco }}, Nº {{ $paciente->numero }}</td>
											<td>{{ $paciente->celular }}</td>
											<td>{{ $paciente->cpf }}</td>
											<td class="text-center">
												<input type="radio" class="btn-check p-0 m-0" @if(isset($_GET['id'])) @if($_GET['id'] == $paciente->id) checked @endif @endif name="paciente_id" id="primary-outlined_{{ $paciente->id }}" autocomplete="off" value="{{ $paciente->id }}">
												<label class="btn btn-outline-primary p-0 px-1 m-0 text-white" for="primary-outlined_{{ $paciente->id }}"><i class="bi bi-check"></i></label>
											</td>
										</tr>
									@endforeach
								</tbody>
							  </table>
						</div>
					</div>
					<div class="modal-footer">
				  		<button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
				  		<button type="button" id="btnConfirmarPaciente" data-bs-dismiss="modal" onclick="pegaValorPaciente()" class="btn btn-dark">Confirmar</button>
					</div>	  
			  </div>
			</div>
		  </div>
			  
		  @endif
		<div class="row justify-content-center align-items-center">
			<div class="col-8">
				@if (Auth::user()->admin == 1)
					<label for="paciente" class="form-label">Paciente</label>
					<div class="input-group mb-3">
						<input type="text" id="paciente" class="form-control rounded-3" placeholder="Selecione um Paciente" disabled value="@if(isset($_GET['id']))@foreach($pacientes as $paciente)@if($_GET['id'] == $paciente->id){{$paciente->nome}} | {{$paciente->cpf}} | {{$paciente->celular}}@endif @endforeach @endif">
						<button class="btn btn-primary" type="button" id="btnModalPacientes" data-bs-toggle="modal" data-bs-target="#modal-pacientes">Selecionar</button>
					</div>
				@endif
				<label for="categoria" class="form-label">Especialidade</label>
				<select class="form-select form-select mb-3" name="categoria" id="categoria">
					<option>Selecione</option>
					@foreach($especialidades as $especialidade)
						<option value="{{ $especialidade->id }}">{{ $especialidade->nome }}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="row justify-content-center align-items-center">
			<div class="form-group col-8">
				<h1 class="text-muted text-center" id="teste">
					Selecione uma Especialidade
				</h1>
				<body>
					<div id='wrap' class="rounded-3 bg-white shadow-sm">
						<div id='calendar'></div>
						<div style='clear:both'></div>
					</div>
				</body>
			</div>
			<hr class="my-3">
		</div>
@endsection

@section('body')
	<script>
		$("#categoria").change(function() {
			
			var _especialidade = $("#categoria").val();
			$("#teste").html("<div class='d-flex align-items-center'><strong>Carregando...</strong><div class='spinner-border ms-auto' role='status' aria-hidden='true'></div></div>");
			$.post("{{ route('agendamentos.datas') }}",
				{
					especialidade : _especialidade
				},
				function (data) {
					if(data == "Nenhum horário encontrado.") {
						$("#calendar").empty();
						$("#wrap").removeClass('p-3');
						$("#teste").html(data);
						$("#teste").show('slow');
					}
					else {
						$("#calendar").empty();
						$("#wrap").addClass('p-3');
						var Calendar = FullCalendar.Calendar;
					
						var calendarEl = document.getElementById('calendar');
						var calendar = new Calendar(calendarEl, {
							contentHeight: 500,
							plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
							header: {
							left: 'prev,next today',
							center: 'title',
							right: 'dayGridMonth'
							},
							locale: 'pt-br',
							editable: true,
							droppable: true,
							selectable: true,
							events: data,
							dateClick: function(info) {
								info.jsEvent.preventDefault();
								$("#div_horarios").html("<div class='d-flex mt-3 align-items-center'><strong>Carregando...</strong><div class='spinner-border ms-auto' role='status' aria-hidden='true'></div></div>")
								$.post("{{ route('agendamentos.horarios') }}",
									{
										especialidade : $("#categoria").val(),
										data : info.dateStr
									},
									function (data) {
										console.log(data);
										setTimeout(() => {
											$("#div_horarios").html("<label for='horario'>Data</label><input type='date' id='horario' class='form-control mb-3' value='"+info.dateStr+"' disabled><input type='hidden' id='horario_id' name='horario_id'>"+data);
											
										}, 1000);
									},
									"json"
								);
								$("#modal-data").modal('show');
								
								

						}
						});
	
						calendar.render();
						//
						$("#teste").hide('slow')
						$("#calendar").slideDown('slow');
					}

				},
				"json"
			);
			
		});

		function pegaValorPaciente() {

			if ($("input[name='paciente_id']").is(":checked")) {
				var user_id = $("input[name='paciente_id']:checked").val();
			}
			
			if(isNaN(user_id)) {
				alert("Selecione um Paciente!");
				return;
			}

			$.post("{{ route('agendamentos.paciente') }}",
				{
					_user_id : user_id
				},
				function (data) {
					$("#paciente").val(data);
					$("#user_id").val(user_id);
				},
				"json"
			);
			
		}

		function confirmar() {
			if (! $("input[name='horario_id']").is(":checked")) {
				alert("Nenhum horario selecionado!");
				return;
			}
			if($("input[name='paciente_id']").length) {
				if ( $("input[name='user_id']") == '') {
					alert("Nenhum paciente selecionado!");
					$("#modal-data").modal('hide');
					$("#btnModalPacientes").focus();
					$("#paciente").focus();
					return;
				}
			}

			$("#form_agendar").submit();
		}

		
	</script>
@endsection