@extends('admin.layout.app')

@section('title', "$agendamento->id - {$agendamento->especialidade->nome} ")

@section('content')
    <h2 class="display-5 text-center">Agendamento - {{ $agendamento->especialidade->nome }}</h2>
    <hr>
    <div class="alert alert-warning text-center" role="alert">
        <i class="bi bi-stopwatch"></i> Compareça ao local com <b>10 minutos</b> de antecedência. <br>
        <i class="bi bi-exclamation-triangle-fill"></i> Atente-se ao uso de máscara! Neste local é <b>obrigatório.</b>
    </div>
    <table class="table table-hover table-sm table-bordered">
        <thead>
          <tr class="table-dark">
            <th scope="col">#</th>
            <th scope="col">Médico</th>
            <th scope="col">Especialidade</th>
            <th scope="col">Data</th>
            <th scope="col">Hora</th>
            <th scope="col">Local</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">{{ $agendamento->id }}</th>
            <td>{{ $agendamento->medico->nome }}</td>
            <td>{{ $agendamento->especialidade->nome }}</td>
            <td>
                {{ $agendamento->horario->data->format('d/m/Y') }}
            </td>
            <td>
              Das {{ $agendamento->horario->h_inicio->format('H:i') }} às {{ $agendamento->horario->h_final->format('H:i') }}
            </td>
            <td>{{ $agendamento->unidade->nome }}</td>
          </tr>
        </tbody>
      </table>

@endsection