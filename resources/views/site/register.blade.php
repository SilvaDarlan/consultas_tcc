@extends('site.template')

@section('title', 'Registrar')

@include('admin.layout.templates.navbar')
@section('content')
<h2 class="display-5 text-center">Cadastro de Usuário</h2>
<hr>
<form action="{{route('site.user.store')}}" method="POST">
    @csrf
    
    <div class="row justify-content-center">
        @if ($errors->any())
		<div class="alert alert-danger">
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			<strong>Ops!</strong> Não foi possível realizar esse cadastro.
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		</div>
	@endif
        <div class="form-group col-7 shadow bg-white px-5 py-3" style="border-radius: 12px">
            <label for="nome" class="form-label">Nome</label>
            <div class="input-group mb-3">
                <input type="text" name="nome" id="nome" class="form-control">
                <span class="input-group-text" id="basic-addon3">
                    <i class="bi bi-person-fill"></i>
                </span>
            </div>
          
            <label for="email" class="form-label">E-mail</label>
            <div class="input-group mb-3">
                <input type="text" name="email" id="email" class="form-control">
                <span class="input-group-text">
                    <i class="bi bi-envelope-fill"></i>
                </span>
            </div>

            <div class="row mb-3">
                <div class="col-6">
                    <label for="password" class="form-label">Senha</label>
                    <div class="input-group">
                        <input type="password" name="password" id="password" class="form-control">
                        <span class="input-group-text">
                            <i class="bi bi-key-fill"></i>
                        </span>
                    </div>
                </div>
                <div class="col-6">
                    <label for="password_confirmation" class="form-label">Confirmar Senha</label>
                    <div class="input-group">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                        <span class="input-group-text">
                            <i class="bi bi-key-fill"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-5">
                    <label for="endereco" class="form-label">Endereço</label>
                    <input type="text" name="endereco" id="endereco" class="form-control">
                </div>
            
                <div class="col-2">
                    <label for="numero" class="form-label">N°</label>
                    <input type="text" name="numero" id="numero" class="form-control">
                </div>
            
                <div class="col-5">
                    <label for="bairro" class="form-label">Bairro</label>
                    <input type="text" name="bairro" id="bairro" class="form-control">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-6">
                    <label for="cep" class="form-label">CEP</label>
                    <input type="text" name="cep" id="cep" class="form-control">
                </div>
                <div class="col-6">
                    <label for="celular" class="form-label">Celular</label>
                    <div class="input-group">
                        <input type="text" name="celular" id="celular" class="form-control">
                        <span class="input-group-text">
                            <i class="bi bi-whatsapp"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                
                <div class="col-6">
                    <label for="cpf" class="form-label">CPF</label>
                    <div class="input-group">
                        <input type="text" name="cpf" id="cpf" class="form-control">
                        <span class="input-group-text">
                            <i class="bi bi-card-heading"></i>
                        </span>
                    </div>
                </div>
                
                <div class="col-6">
                    <label for="rg" class="form-label">RG</label>
                    <div class="input-group">
                        <input type="text" name="rg" id="rg" class="form-control">
                        <span class="input-group-text">
                            <i class="bi bi-card-heading"></i>
                        </span>
                    </div>
                </div>
            </div>

            <hr>

            <div class="form-group text-center mt-3">
                <div class="col">
                    <button type="submit" class="btn btn-primary px-5" title="Cdastrar Usuário"> <i class="bi bi-person-check-fill"></i> Concluir Cadastro</button>
                </div>
                <a href=" {{ route('login') }} " class="">Fazer Login</a>
            </div>
        </div>
    </div>
</form>
    
@endsection
@section('body')
    <script>
        $(document).ready(function () {
            $('#celular').mask('(00) 00000-0000');
            $('#cep').mask('00000-000');
            $('#cpf').mask('000.000.000-00');
            $('#rg').mask('00.000.000-0');
        });
    </script>
@endsection