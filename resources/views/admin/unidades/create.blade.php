@extends('admin.layout.app')

@section('title', 'Nova Unidade de Atendimento')
<style>
	@media(max-width: 960px)
	{
		#coluna-principal {
			width: 100%;
		}
	}
</style>
@section('content')
	@section('nav')
		<li class="nav-item">
			<a class="nav-link" aria-current="page" href="{{route('agendamentos.index')}}">Início</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" aria-current="page" href="{{route('unidades.list')}}">Unidades</a>
		</li>
		<li class="nav-item">
			<a class="nav-link bg-light active" aria-current="page" href="{{route('unidades.create')}}">Cadastrar Unidade</a>
		</li>
	@endsection
	<h2 class="display-5 text-center">Cadastro de Unidade</h2>
	<hr>
    <form action="{{ route('unidades.store') }}" method="POST">
		@csrf
		
		<div class="row justify-content-center">
			<div class="form-group col-6" id="coluna-principal">
			  	<label for="nome" class="form-label">Nome</label>
				<input type="text" name="nome" id="nome" class="form-control mb-3">
				<div class="row">
					<div class="col-9">
						<label for="nome" class="form-label">Endereço</label>
						<input type="text" name="endereco" id="endereco" class="form-control mb-3">
					</div>
					<div class="col-3">
						<label for="nome" class="form-label">Número</label>
						<input type="text" name="numero" id="numero" class="form-control mb-3">
					</div>
				</div>

				<div class="row">
					<div class="col-8">
						<label for="nome" class="form-label">Bairro</label>
						<input type="text" name="bairro" id="bairro" class="form-control mb-3">
					</div>
					<div class="col-4">
						<label for="nome" class="form-label">CEP</label>
						<input type="text" name="cep" id="cep" class="form-control mb-3">
					</div>
				</div>

				<label for="nome" class="form-label">Contato</label>
				<input type="text" name="contato" id="contato" class="form-control mb-3" placeholder="Telefone, Email ...">

				<label for="local" class="form-label text-muted local_label" >Link (opcional) <a href="" id="obter_local" class="fw-bold" style="text-decoration: none;">Obter</a></label>
				<input type="text" name="local" id="local" class="form-control mb-3 local_label" placeholder="www.google.com.br/maps">
			</div>

			<hr>

			<div class="form-group text-center">
				<button type="submit" class="btn btn-dark px-5" title="Cdastrar Unidade"> <i class="bi bi-geo-alt-fill"></i> Concluir Cadastro</button>
			</div>
		</div>
	</form>
	
@endsection
@section('body')
	<script>
		$("#obter_local").click(function() {
			var rua = $("#endereco").val();
			var numero = $("#numero").val();
			var bairro = $("#bairro").val();
			var cep = $("#cep").val();
			if(rua == '' || numero == '' || bairro == '' || cep == '') {
				alert('Primeiro informe o endereço da unidade');
				return;
			}
			$("#obter_local").attr('href', 'https://www.google.com.br/maps/dir//R,+'+rua+',+'+numero+',+Guararapes+-+SP+'+cep);
			$("#obter_local").attr('target', '_blank');
		})
	</script>
@endsection