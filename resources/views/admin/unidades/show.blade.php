@extends('admin.layout.app')

@section('title', "Unidade - $unidade->nome")

@section('content')
    <h2 class="display-5 text-center">Unidade - {{ $unidade->nome }}</h2>
    <hr>
    <h5 class="text-center">
        <a href="{{ $unidade->local }}" class="text-decoration-none">Ver {{ $unidade->nome }} no mapa</a>
    </h5>

@endsection