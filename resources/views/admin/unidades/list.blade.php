@extends('admin.layout.app')

@section('title', 'Unidades')
<style>
  .cadastrar {
transition: all .3s;
}
.cadastrar:hover {
text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
font-size: 30px;
}

</style>

@section('content')
  @section('nav')
    <li class="nav-item">
      <a class="nav-link" aria-current="page" href="{{route('agendamentos.index')}}">Início</a>
    </li>
    <li class="nav-item">
      <a class="nav-link bg-light active" aria-current="page" href="{{route('unidades.list')}}">Unidades</a>
    </li>
  @endsection
    <h2 class="display-5 text-center">Unidades de Atendimento</h2>
    <hr>
    <div class="text-center">
      <a style="text-decoration: none; color: #000;" href="{{ route('unidades.create') }}" class="cadastrar h4 p-3 text-muted" title="Cadastrar Nova Unidade"> <i class="bi bi-plus-lg"></i> Cadastrar Unidade</a>
    </div>
    <hr>
    @if($mensagem = Session::get('mensagem'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{ $mensagem }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
    </div>
    @endif
    <div class="table-responsive-lg">
      <table class="table table-hover">
        <thead>
          <tr class="table-dark">
            <th scope="col" nowrap>#</th>
            <th scope="col" nowrap>Nome</th>
            <th scope="col" nowrap>Endereço</th>
            <th scope="col" nowrap>Bairro</th>
            <th scope="col" nowrap>CEP</th>
            <th scope="col" nowrap>Contato</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($unidades as $unidade)
            <tr class="body @if($mensagem && ($id = Session::get('id')) == $unidade->id) text-success fw-bold @endif" >
                <th nowrap scope="row">{{$unidade->id}}</th>
                <td nowrap>{{$unidade->nome}}</td>
                <td nowrap><a href="@if($unidade->local != '') {{ $unidade->local }} @else https://www.google.com.br/maps/search/R,+{{$unidade->endereco}},+'{{$unidade->numero}}',+Guararapes+-+SP+{{$unidade->cep}} @endif" id="mapa" target="_blank" style="text-decoration: none" title="Ver {{$unidade->nome}} no mapa">{{$unidade->endereco}}, {{$unidade->numero}}</a></td>
                <td nowrap>{{$unidade->bairro}}</td>
                <td nowrap>{{$unidade->cep}}</td>
                <td nowrap>{{$unidade->contato}}</td>
            @endforeach
        </tbody>
      </table>
    </div>
@endsection
