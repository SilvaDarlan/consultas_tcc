@extends('admin.layout.app')

@section('title', 'Médico')

@section('content')
  @section('nav')
    <li class="nav-item">
      <a class="nav-link" aria-current="page" href="{{route('agendamentos.index')}}">Início</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" aria-current="page" href="{{route('medicos.list')}}">Médicos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link bg-light active" aria-current="page" href="{{route('medicos.create')}}">Cadastrar Médico</a>
    </li>
  @endsection
	<h2 class="display-5 text-center">Dr(a). {{ $medico->nome }}</h2>
	<hr>
    <form enctype="multipart/form-data" action="{{ route('medicos.update', $medico->id) }}" method="POST">
		@csrf
    @method('put')
		
		<div class="row justify-content-center">
			<div class="form-group col-9">
        @if($mensagem = Session::get('mensagem'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>{{ $mensagem }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </div>
        @endif
			  <label for="nome" class="form-label">Nome</label>
        <input type="text" name="nome" id="nome" class="form-control" value="{{$medico->nome}}">

        <label for="email" class="form-label">Email</label>
        <input type="text" name="email" id="email" class="form-control" value="{{$medico->email}}">
        
        <label for="telefone" class="form-label">Telefone</label>
        <input type="text" name="telefone" id="telefone" class="form-control" value="{{$medico->telefone}}">
        <div class="row mb-3">

          <div class="col-4">
            <label for="CRM" class="form-label">CRM</label>
            <input type="text" name="CRM" id="CRM" class="form-control" value="{{$medico->crm}}">
          </div>
          
          <div class="col-4">
            <label for="CPF" class="form-label">CPF</label>
            <input type="text" name="CPF" id="CPF" class="form-control" value="{{$medico->cpf}}">
          </div>
          
          <div class="col-4">
            <label for="RG" class="form-label">RG</label>
            <input type="text" name="RG" id="RG" class="form-control" value="{{$medico->rg}}">
          </div>
        </div>

        <div class="row">
          <div class="col-6">
            <label for="especialidade_id" class="form-label">Especialidade</label>
            <div class="input-group mb-3">
                <button type="button" class="btn btn-secondary col-12" data-bs-toggle="modal" data-bs-target="#exampleModal">
                  Slecionar Especialidades
                </button>
            </div>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Selecionar Especialidades</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    @foreach($especialidades as $especialidade)
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="{{$especialidade->id}}" id="especialidade_ids[]" name="especialidade_ids[]" @foreach($med_esps as $esps) @if($esps->especialidade_id == $especialidade->id) disabled @endif @endforeach>
                          <label class="form-check-label" for="especialidade_ids[]">
                            {{$especialidade->nome}}
                          </label>
                        </div>
                    @endforeach
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Confirmar</button>
                  </div>
                </div>
              </div>
            </div>


            </select>
          </div>

          <div class="col-6">
            <label for="unidade_id" class="form-label">Unidade de Atendimento</label>
            <select class="form-select form-select mb-3" name="unidade_id" id="unidade_id">
              @foreach($unidades as $unidade)
                <option @if($medico->unidade->id == $unidade->id) selected @endif value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <hr>

				<div class="form-group text-center">
					<button type="submit" class="btn btn-secondary px-5" title="Alterar Cadastro"> <i class="bi bi-person-check-fill"></i> Salvar Alterações</button>
				</div>
			</div>
		</div>
	</form>
	
@endsection