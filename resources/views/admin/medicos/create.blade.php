@extends('admin.layout.app')

@section('title', 'Novo Horário')

@section('content')
  @section('nav')
    <li class="nav-item">
      <a class="nav-link" aria-current="page" href="{{route('agendamentos.index')}}">Início</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" aria-current="page" href="{{route('medicos.list')}}">Médicos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link bg-light active" aria-current="page" href="{{route('medicos.create')}}">Cadastrar Médico</a>
    </li>
  @endsection
	<h2 class="display-5 text-center">Cadastro de Médico</h2>
	<hr>
    <form action="{{ route('medicos.store') }}" method="POST">
		@csrf
		
		<div class="row justify-content-center">
			<div class="form-group col-9">
        <div class="row mb-3">
          <div class="col">
            <label for="nome" class="form-label">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control">
          </div>
          <div class="col">
            <label for="email" class="form-label">Email</label>
            <input type="text" name="email" id="email" class="form-control">
          </div>
            
        </div>
        
        
        <div class="row mb-3">
          <div class="col">
            <label for="celular" class="form-label">Celular</label>
            <input type="text" name="telefone" id="celular" class="form-control">
          </div>
          
          <div class="col">
            <label for="crm" class="form-label">CRM/UF</label>
            <input type="text" name="crm" id="crm" class="form-control">
          </div>
        </div>
        <div class="row mb-3">
          <div class="col">
            <label for="cpf" class="form-label">CPF</label>
            <input type="text" name="cpf" id="cpf" class="form-control">
          </div>
          
          <div class="col">
            <label for="rg" class="form-label">RG</label>
            <input type="text" name="rg" id="rg" class="form-control">
          </div>
        </div>
        <div class="modal fade" tabindex="-1" id="modalNovaEspecialidade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Cadastrar Especialidade</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <label for="recipient-name" class="col-form-label">Especialidade:</label>
                <input type="text" class="form-control" id="nova_especialidade">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary" onclick="novaEspecialidade();">Cadastrar</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <label for="especialidade_id" class="form-label">Especialidade <span class="text-decoration-none fw-bold p-2 h5" style="cursor: pointer" id="nova_especialidade" title="Cadastrar Especialidade" data-bs-toggle="modal" data-bs-target="#modalNovaEspecialidade">+</span></label>
            <select class="form-select form-select mb-3" name="especialidade_id" id="especialidade_id">
              
              @foreach($especialidades as $especialidade)
                <option value="{{ $especialidade->id }}">{{ $especialidade->nome }}</option>
              @endforeach

            </select>
          </div>

          <div class="col-6">
            <label for="unidade_id" class="form-label">Unidade de Atendimento</label>
            <select class="form-select form-select mb-3" name="unidade_id" id="unidade_id">
              @foreach($unidades as $unidade)
                <option value="{{ $unidade->id }}">{{ $unidade->nome }}</option>
              @endforeach
            </select>
          </div>
        </div>

        <hr>

				<div class="form-group text-center">
					<button type="submit" class="btn btn-dark px-5" title="Cdastrar Médico"> <i class="bi bi-person-check-fill"></i> Concluir Cadastro</button>
				</div>
			</div>
		</div>
	</form>
  @endsection
  @section('body')
    <script>
        $(document).ready(function () {
            $('#celular').mask('(00) 00000-0000');
            $('#crm').mask('00000000-0/AA');
            $('#cpf').mask('000.000.000-00');
            $('#rg').mask('00.000.000-0');
        });

        function novaEspecialidade() {
          var especialidade = $("#nova_especialidade").val();
          $.post("{{ route('especialidades.create') }}",
            {
              nome : especialidade
            },
            function (data) {
              var especialidade = data['nome'];
              var id = data['id']
              $("#especialidade_id").append("<option value='"+id+"' selected>"+especialidade+"</option>");
              $("#modalNovaEspecialidade").modal('hide');
            },
            "json"
          );
        }
    </script>
  @endsection