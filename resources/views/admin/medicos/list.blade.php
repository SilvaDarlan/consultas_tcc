@extends('admin.layout.app')

@section('title', 'Médicos')
<style>
  .cadastrar {
  transition: all .3s;
  }
  .cadastrar:hover {
  text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
  font-size: 30px;
  }
</style>

@section('content')
	<h2 class="display-5 text-center">Lista de Médicos</h2>
	<hr>
  <div class="text-center">
    <a style="text-decoration: none; color: #000;" href="{{ route('medicos.create') }}" class="cadastrar h4 p-3 text-muted" title="Cadastrar Médico"> <i class="bi bi-person-plus-fill"></i> Cadastrar Médico</a>
  </div>
  <hr>
  @if($mensagem = Session::get('mensagem'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<p>{{ $mensagem }}</p>
		<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
	</div>
  @endif
  @if (count($medicos) > 0) 
  <div class="table-responsive-lg">
	  <table class="table table-hover">
		  <thead>
			<tr class="table-dark">
			  <th scope="col" nowrap>#</th>
			  <th scope="col" nowrap>Nome</th>
			  <th scope="col" nowrap>E-mail</th>
			  <th scope="col" nowrap>CRM</th>
			  <th scope="col" nowrap>Especialidades</th>
			  <th scope="col" nowrap>Unidade de Atendimento</th>
			  <th nowrap>Operações</th>
			</tr>
		  </thead>
		  <tbody>
			  @foreach ($medicos as $medico)
			  <tr class="@if( ($id = Session::get('id') ) == $medico->id ) text-success fw-bold @endif">
				  <th nowrap scope="row">{{$medico->id}}</th>
				  <td nowrap>{{$medico->nome}}</td>
				  <td nowrap>{{$medico->email}}</td>
				  <td nowrap>{{$medico->crm}}</td>
				  <td nowrap>
					  @php
						  $primeiro = true;
					  @endphp
					  @foreach ($medico->especialidades as $especialidade)
						  @if (!$primeiro)| 
						  @endif
						  @php
							  $primeiro = false;
						  @endphp
	
						  {{$especialidade->nome}}
					  @endforeach
				  </td>
				  <td nowrap><a href="@if($medico->unidade->local != '') {{ $medico->unidade->local }} @else https://www.google.com.br/maps/search/R,+{{$medico->unidade->endereco}},+'{{$medico->unidade->numero}}',+Guararapes+-+SP+{{$medico->unidade->cep}} @endif" id="mapa" target="_blank" style="text-decoration: none" title="Ver {{$medico->unidade->nome}} no mapa">{{$medico->unidade->nome}}</a></td>
				  <td nowrap>
					  <div class="row">
						  <div class="col m-0 p-0 d-flex">
							  <a href="{{route('medicos.edit', $medico->id)}}" class="btn btn-secondary btn-sm mx-1">Editar</a>
								<form action="{{route('medicos.delete', $medico->id)}}" method="POST" class="m-0 p-0">
									@method('delete')
									@csrf
									<button class="btn btn-danger btn-sm mx-1" type="button" data-bs-toggle="modal" data-bs-target="#apagarMedicoModal{{$medico->id}}">Apagar</button>
									<div class="modal fade" id="apagarMedicoModal{{$medico->id}}" tabindex="-1" aria-labelledby="apagarMedicoModal{{$medico->id}}Label" aria-hidden="true">
										<div class="modal-dialog">
										  <div class="modal-content">
											<div class="modal-header">
											  <h5 class="modal-title" id="apagarMedicoModal{{$medico->id}}Label">Deseja realmente excluir esse Cadastro?</h5>
											  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
											</div>
											<div class="modal-body text-center">
											  <p class=""><b>Dr(a). {{ $medico->nome }}</b></p>
											  <p class="text-muted p-0 m-0">
													@php
														$primeiro = true;
													@endphp
													@foreach ($medico->especialidades as $especialidade)
														@if (!$primeiro)| 
														@endif
														@php
															$primeiro = false;
														@endphp
														{{$especialidade->nome}}
													@endforeach
											  </p>
											  <p class="text-muted p-0 m-0"><b>Contato:</b> {{ $medico->email }}, <b>CRM:</b> {{ $medico->crm }}</p>
											  <p class="text-muted p-0 m-0"><i class="bi bi-geo-alt-fill"></i> {{ $medico->unidade->nome }}</p>
											</div>
											<div class="modal-footer">
											  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
											  <button type="submit" class="btn btn-outline-danger" id="btnApagarMedico">Apagar</button>
											</div>
										  </div>
										</div>
									  </div>
								</form>
							</div>
					</div>
				  </td>
			  </tr>
			  @endforeach
		  </tbody>
		</table>
  </div>
  @else
  <h1 class="text-muted text-center" id="teste">
    Nenhum Médico Encontrado
  </h1>
  @endif

	
@endsection