@extends('admin.layout.app')

@section('title', "Paciente #{$paciente->id} ")
<style>

</style>
@section('content')
    <h2 class="display-5 text-center" style="font-weight: 400">
        @if (Auth::user()->admin == 1)
            Paciente #{{ $paciente->id }}
        @else
            Meus Dados
        @endif
    </h2>
    <hr>
    @if($mensagem = Session::get('mensagem'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>{{ $mensagem }}</p>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
        </div>
    @endif
    @php
        $status = '';
    @endphp
    <div class="col-9 mx-auto">
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="nome" class="form-label">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" value="{{ $paciente->nome }}">
                  </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $paciente->email }}">
                  </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="mb-3">
                    <label for="endereco" class="form-label">Endereco</label>
                    <input type="text" class="form-control" id="endereco" name="endereco" value="{{ $paciente->endereco }}">
                  </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="numero" class="form-label">Nº</label>
                    <input type="text" class="form-control" id="numero" name="numero" value="{{ $paciente->numero }}">
                  </div>
            </div>
            <div class="col">
                <div class="mb-3">
                    <label for="bairro" class="form-label">Bairro</label>
                    <input type="text" class="form-control" id="bairro" name="bairro" value="{{ $paciente->email }}">
                  </div>
            </div>
        </div>
    </div>
    <hr>
    @if (Auth::user()->admin == 1)
        
        <h4 class="text-muted justify-content-between d-flex"><span><i class="bi bi-clock-history"></i> Histórico de Consultas </span><a href="{{ route('agendamentos.create') }}?id={{$paciente->id}}" id="btnNovaConsulta" class="btn btn-sm btn-primary" title="Marcar para este Paciente"><i class="bi bi-plus"></i>Marcar Consulta</a></h4>
        @if (count($agendamentos) > 0)
        <div class="table-responsive-xl">
            <table class="table" id="table_historico">
                <tbody>
                    <tr>
                        <td colspan="6"></td>
                    </tr>
                    @foreach ($agendamentos as $agendamento)
                        <tr class="body">
                            <th nowrap scope="row">{{ $agendamento->especialidade->nome }} </th>
                            <td nowrap><i class="bi bi-person-badge-fill"></i> Dr(a). {{ $agendamento->medico->nome}} </td>
                            <td nowrap><i class="bi bi-calendar"></i> {{ $agendamento->horario->dia_semana }}, {{ $agendamento->horario->data->format('d/m/Y') }} </td>
                            <td nowrap><i class="bi bi-clock"></i> Entre {{ $agendamento->horario->h_inicio->format('H:i') }} e {{ $agendamento->horario->h_final->format('H:i') }} </td>
                            <td nowrap><i class="bi bi-geo-alt-fill"></i> {{ $agendamento->unidade->nome }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
            <h5 class="text-muted text-center">@if(Auth::user()->admin == 1) Este paciente @else Você @endif não tem nunhuma consulta finalizada.</h5>
        @endif
        <div class="collapse" id="agendamentos">
            <div class="table-responsive-xl">
                <table class="table" id="table_historico">
                    <tbody>
                        @foreach ($agendamentos2 as $agendamento2)
                            @if ($status != $agendamento2->status)
                                <tr>
                                    <td colspan="6" class="fw-bold text-muted">{{ $status = $agendamento2->status }}</td>
                                </tr>
                            @endif
                            <tr class="body">
                                <th nowrap scope="row">{{ $agendamento2->especialidade->nome }} </th>
                                <td nowrap><i class="bi bi-person-badge-fill"></i> Dr(a). {{ $agendamento2->medico->nome}} </td>
                                <td nowrap><i class="bi bi-calendar"></i> {{ $agendamento2->horario->dia_semana }}, {{ $agendamento2->horario->data->format('d/m/Y') }} </td>
                                <td nowrap><i class="bi bi-clock"></i> Entre {{ $agendamento2->horario->h_inicio->format('H:i') }} e {{ $agendamento2->horario->h_final->format('H:i') }} </td>
                                <td nowrap><i class="bi bi-geo-alt-fill"></i> {{ $agendamento2->unidade->nome }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @if (count($agendamentos2) > 0)
            <button class="btn btn-secondary btn-sm" id="btnVerMais" data-bs-toggle="collapse" data-bs-target="#agendamentos" aria-expanded="false" aria-controls="agendamentos">Ver mais</button>
        @endif
    @endif
@endsection
@section('body')
    <script>
        $('#celular').mask('(00) 00000-0000');
        $('#cep').mask('00000-000');
        $('#cpf').mask('000.000.000-00');
        $('#rg').mask('00.000.000-0');
        
    </script>
@endsection

