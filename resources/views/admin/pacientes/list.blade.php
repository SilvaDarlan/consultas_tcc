@extends('admin.layout.app')

@section('title', 'Lista de Pacientes')

<style>
  .agendamento:hover {
    background: rgba(0, 0, 0, 0.1);

  }
  .agendamento {
    transition: background .5s;
  }
  /*******/
  .agendar {
    transition: font-size .3s;
  }
  .agendar:hover {
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
    font-size: 30px;
  }
  /*******/
  .ver {
    transition: font-size .2s;
  }
  .ver:hover {
    font-size: 60px;
  }
  /*******/

</style>
@section('content')
    <h2 class="display-5 text-center" style="font-weight: 400">Pacientes</h2>
    <hr>
    <div class="text-center">
      <a style="text-decoration: none; color: #000;" href="{{ route('pacientes.create') }}" class="agendar h4 p-3 text-muted" title="Cadastrar Novo Paciente"> <i class="bi bi-plus-lg"></i> Cadastrar Paciente</a>
    </div>
    <hr>
	@if($mensagem = Session::get('mensagem'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <p>{{ $mensagem }}</p>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
    </div>
    @endif
	@if (count($pacientes) > 0)
	<div class="table-responsive-xxl">
		<table class="table table-hover">
			<thead>
			  <tr class="table-dark">
				<th nowrap scope="col">#</th>
				<th nowrap scope="col">Nome</th>
				<th nowrap scope="col">Email</th>
				<th nowrap scope="col">Endereço</th>
				<th nowrap scope="col">Bairro</th>
				<th nowrap scope="col">CEP</th>
				<th nowrap scope="col">RG</th>
				<th nowrap scope="col">CPF</th>
				<th nowrap scope="col">Celular</th>
				<th nowrap scope="col">Operações</th>
			  </tr>
			</thead>
			<tbody>
				@foreach ($pacientes as $paciente)
				<tr class="@if($mensagem && ($id = Session::get('id')) == $paciente->id) text-success fw-bold @endif">
				  <th nowrap scope="row">{{ $paciente->id }}</th>
				  <td nowrap>{{ $paciente->nome }}</td>
				  <td nowrap>{{ $paciente->email }}</td>
				  <td nowrap>{{ $paciente->endereco }}, {{ $paciente->numero }}</td>
				  <td nowrap>{{ $paciente->bairro }}</td>
				  <td nowrap>{{ $paciente->cep }}</td>
				  <td nowrap>{{ $paciente->rg }}</td>
				  <td nowrap>{{ $paciente->cpf }}</td>
				  <td nowrap>{{ $paciente->celular }}</td>
				  <td nowrap>
					<div class="row">
						<div class="col m-0 p-0 d-flex">
							<a href="{{route('pacientes.show', $paciente->id)}}" class="btn btn-secondary btn-sm mx-1">Editar</a>
							  <form action="{{route('pacientes.delete', $paciente->id)}}" method="POST" class="m-0 p-0">
								  @method('delete')
								  @csrf
								  <button class="btn btn-danger btn-sm mx-1" type="button" data-bs-toggle="modal" data-bs-target="#apagarPaciente{{$paciente->id}}">Excluir</button>
								  <div class="modal fade" id="apagarPaciente{{$paciente->id}}" tabindex="-1" aria-labelledby="apagarPaciente{{$paciente->id}}Label" aria-hidden="true">
									  <div class="modal-dialog">
										<div class="modal-content">
										  <div class="modal-header">
											<h5 class="modal-title" id="apagarPaciente{{$paciente->id}}Label">Deseja realmente excluir esse Cadastro?</h5>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										  </div>
										  <div class="modal-body text-center">
											<p class=""><b>#{{ $paciente->id }} {{ $paciente->nome }}</b></p>
											<p class="text-muted p-0 m-0"><b>Email:</b> {{ $paciente->email }}, <b>Celular:</b> {{ $paciente->celular }}</p>
											<p class="text-muted p-0 m-0">{{ $paciente->endereco }}, Nº {{ $paciente->numero }}</p>
											<p class="text-muted p-0 m-0">{{ $paciente->bairro }} - {{ $paciente->cep }}</p>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
											<button type="submit" class="btn btn-outline-danger" id="btnApagarPaciente">Excluir</button>
										  </div>
										</div>
									  </div>
									</div>
							  </form>
						  </div>
				  </div>
				  </td>
				</tr>
				@endforeach
			</tbody>
		  </table>
	</div>
	@else
	  <h4 class="text-muted text-center">Nnehum Paciente Encontrado.</h4>
	@endif

@endsection

