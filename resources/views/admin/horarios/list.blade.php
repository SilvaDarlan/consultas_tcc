@extends('admin.layout.app')
@php
    $mes = array( '01' => 'Janeiro', '02' => 'Fevereiro', '03' => 'Março', '04' => 'Abril', '05' => 'Maio', '06' => 'Junho', '07' => 'Julho', '08' => 'Agosto', '09' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro');
@endphp
@section('title', 'Horarios')
<style>
      .cadastrar {
    transition: all .3s;
  }
  .cadastrar:hover {
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);
    font-size: 30px;
  }
</style>
@section('content')
    <h2 class="display-5 text-center ">Horários</h2>
    <hr>
    <div class="text-center">
      <a style="text-decoration: none; color: #000;" href="{{ route('horarios.create') }}" class="cadastrar h4 p-3 text-muted" title="Cadastrar Novo Horário"> <i class="bi bi-plus-lg"></i> Cadastrar Horário</a>
    </div>
    <hr>
    @if (count($horarios) > 0)
        
        <table class="table mb-5 mt-5">
            @php
                $dia_semana = '';
                $primeiro = true;
                $mes_ano = '';
            @endphp
            @foreach ($horarios as $horario)
                @if ($horario->vagas > 0)
                    
                    @if ($horario->dia_semana != $dia_semana)
                        @if (!$primeiro)
                            <tr>
                                <td colspan="6">&nbsp;</td>
                            </tr>
                        @endif
                        @php
                            $primeiro = false;
                            $mes_ano = '';
                        @endphp
                        <tr>
                            <td scope="col" colspan="6" class="text-center rounded-top h5 table-dark">{{ $dia_semana = $horario->dia_semana }}</td>
                        </tr>
                        <tr class=" fw-bold text-center" style="background: #dee2e6;">
                            <td>Data</td>
                            <td>Horário</td>
                            <td>Especialidade</td>
                            <td>Médico</td>
                            <td>Vagas Restantes</td>
                            <td>Operações</td>
                        </tr>
                    @endif
                    @if ($mes[ $horario->data->format('m') ] . $horario->data->format(' Y') != $mes_ano)
                    <tr>
                        <td class="text-muted fw-bold bg-light" colspan="6">{{ $mes_ano = $mes[ $horario->data->format('m') ] . $horario->data->format(' Y') }}</td>
                    </tr>
                    @endif
                    <tr class="text-center">
                        <td> {{ $horario->data->format('d/m/Y') }} </td>
                        <td> {{ $horario->h_inicio->format('H:i') }} às {{ $horario->h_final->format('H:i') }} </td>
                        <td> {{ $horario->especialidade->nome }} </td>
                        <td> {{ $horario->medico->nome }} </td>
                        <td> {{ $horario->vagas }} </td>
                        <td>
                            <form action="{{route('horarios.delete', $horario->id)}}" class="m-0 p-0" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal" data-bs-target="#modalApagarHorario{{$horario->id}}">Apagar</button>
                                <div class="modal fade" id="modalApagarHorario{{$horario->id}}" tabindex="-1" aria-labelledby="modalApagarHorario{{$horario->id}}Label" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="modalApagarHorario{{$horario->id}}Label">Deseja realmente apagar este horário?</h5>
                                          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                          @if (($horario->updated_at->format('d/m/Y H:i:s')) <> ($horario->created_at->format('d/m/Y H:i:s')))
                                            <div class="alert alert-warning">
                                                <h5 class="alert-heading">Atenção!</h5>
                                                <hr>
                                                <p>Existem consultas agendadas para este horário.</p>
                                                <p class="text-sm text-muted text-right"><i>Os pacientes serão notificados sobre esta operação.</i></p>
                                            </div>
                                          @endif
                                          <div class="mb-3">
                                            <label for="motivoExclusao" class="form-label">Motivo: </label>
                                            <input type="text" class="form-control" id="motivoExclusao">
                                          </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                                          <button type="submit" class="btn btn-outline-danger">Apagar</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                            </form>
                        </td>
                    </tr>
                @endif
            @endforeach
        </table>
    @else
        <h4 class="text-center text-muted">Nenhum Horário Encontrado.</h4>
    @endif
@endsection