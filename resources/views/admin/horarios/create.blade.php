@extends('admin.layout.app')

@section('title', 'Novo Horário')

@section('content')
  @section('nav')
      <li class="nav-item">
          <a class="nav-link" aria-current="page" href="{{route('agendamentos.index')}}">Início</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" aria-current="page" href="{{route('horarios.list')}}">Horários</a>
      </li>
      <li class="nav-item">
        <a class="nav-link bg-light active" aria-current="page" href="{{route('horarios.create')}}">Cadastrar Horário</a>
    </li>
  @endsection
	<h2 class="display-5 text-center">Novo Horário</h2>
	<hr>
    <form action="{{ route('horarios.store') }}" method="POST">
		@csrf
		
		<div class="row justify-content-center">
			<div class="form-group col-7">
				<div class="row">
          <div class="col">
            <label for="medico_id" class="form-label">Medico</label>
            <select class="form-select form-select mb-3" name="medico_id" id="medico_id">
              <option>Selecionar</option>
              @foreach ($medicos as $medico)
                <option value="{{ $medico->id }}">{{ $medico->nome }} </option>
              @endforeach
            </select>
          </div>
          <div class="col">
            <label for="especialidade_id" class="form-label">Especialidade</label>
            <select class="form-select form-select mb-3" name="especialidade_id" id="especialidade_id" disabled aria-placeholder="Selecione um Médico">
              <option>Selecione um Médico</option>
            </select>
          </div>
        </div>

          <div class="row">
            <div class="col-8">
              <label class="form-label">Dias da Semana</label>
              <div class="form-control">
                  <div class="form-check form-check-inline mx-1">
                      <input class="form-check-input" type="checkbox" id="seg" value="1" name="dia_semana[]">
                      <label class="form-check-label" for="1">Segunda</label>
                    </div>
                    <div class="form-check form-check-inline mx-1">
                      <input class="form-check-input" type="checkbox" id="ter" value="2" name="dia_semana[]">
                      <label class="form-check-label" for="2">Terça</label>
                    </div>
                    <div class="form-check form-check-inline mx-1">
                      <input class="form-check-input" type="checkbox" id="qua" value="3" name="dia_semana[]">
                      <label class="form-check-label" for="3">Quarta</label>
                    </div>
                    <div class="form-check form-check-inline mx-1">
                      <input class="form-check-input" type="checkbox" id="qui" value="4" name="dia_semana[]">
                      <label class="form-check-label" for="4">Quinta</label>
                    </div>
                    <div class="form-check form-check-inline mx-1">
                      <input class="form-check-input" type="checkbox" id="sex" value="5" name="dia_semana[]">
                      <label class="form-check-label" for="5">Sexta</label>
                    </div>
              </div>
            </div>
            <div class="col-4">
              <label for="data" class="form-label">Mês</label>
              <input type="month" id="data" name="data" class="form-control">
            </div>
          </div>

            <div class="form-group row mt-3">
              <div class="col-8">
                <label for="especialidade_id" class="form-label">Horário de Atendimento</label>
              </div>
              <div class="col-4">
                <label for="vagas" class="form-label"> N° de Vagas</label>
              </div>
              <div class="col text-center">
                <input type="time" name="h_inicio" id="h_inico" class="form-control">
              </div>
              ás
              <div class="col">
                <input type="time" name="h_final" id="h_final" class="form-control">
              </div>

              <div class="col">
                <input type="text" id="vagas" name="vagas" class="form-control">
              </div>
            </div>

				<hr>

				<div class="form-group text-center">
					<button type="submit" class="btn btn-dark px-5" title="Cadastrar Horário"> <i class="bi bi-calendar-plus"></i> Cadastrar Horário</button>
				</div>
			</div>
		</div>
	</form>
	
@endsection
@section('body')
    <script>
      $("#medico_id").change(function() {

        var medico_id = $("#medico_id").val();
        $("#especialidade_id").html("<option><div class='d-flex align-items-center'><strong>Carregando...</strong><div class='spinner-border ms-auto' role='status' aria-hidden='true'></div></div></option>");

        $.ajax({
          type: "POST",
          url: "{{route('especialidades')}}",
          data: {
            medico_id : medico_id
          },
          dataType: "json",
          success: function (response) {
            $("#especialidade_id").html(response);
            $("#especialidade_id").removeAttr("disabled");
          }
        });
      });
    </script>
@endsection