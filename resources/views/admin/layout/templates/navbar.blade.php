<style>

  .nav-link {
    height: 50px;
    font-weight: 500;
    opacity: .75;
    transition: all .25s;

  }
  .nav-link:hover {
    border-bottom: 3px solid white;
    opacity: 1;
  }

  .dropdown:hover>.dropdown-menu {
     display: block;
}

</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow-sm sticky-top m-0 p-0">
    <div class="container">
		<a class="navbar-brand" href="{{route('agendamentos.index')}}">
			<img src=" {{url('storage/app/public/testeteste.png')}} " width="200">
		</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
			<div class="m-0 p-0 d-flex justify-content-between col-12">
				<ul class="navbar-nav">
					@auth
						@if(Auth::user()->admin == 1)
						<li class="nav-item dropdown">
							<a class="nav-link dropdown text-white agendamentos" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							Agendamentos
							</a>
							<ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								<li><a class="dropdown-item" href="{{ route('agendamentos.create') }}">Marcar Consulta</a></li>
								<li><a class="dropdown-item" href="{{ route('agendamentos.index') }}">Listar Agendamentos</a></li>
							</ul>
						</li>
							<li class="nav-item dropdown">
							<a class="nav-link dropdown text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Horários
							</a>
							<ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								<li><a class="dropdown-item" href="{{ route('horarios.create') }}">Cadastrar Horário</a></li>
								<li><a class="dropdown-item" href="{{ route('horarios.list') }}">Listar Horários</a></li>
							</ul>
							</li>
							<li class="nav-item dropdown">
							<a class="nav-link dropdown text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Pacientes
							</a>
							<ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								<li><a class="dropdown-item" href="{{ route('pacientes.create') }}">Cadastrar Paciente</a></li>
								<li><a class="dropdown-item" href="{{ route('pacientes.list') }}">Listar Pacientes</a></li>
							</ul>
							</li>
	
							<li class="nav-item dropdown">
							<a class="nav-link dropdown text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Médicos
							</a>
							<ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								<li><a class="dropdown-item" href="{{ route('medicos.create') }}">Cadastrar Médico</a></li>
								<li><a class="dropdown-item" href="{{ route('medicos.list') }}">Listar Médicos</a></li>
							</ul>
							</li>
	
							<li class="nav-item dropdown">
							<a class="nav-link dropdown text-white" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								Unidades
							</a>
							<ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								<li><a class="dropdown-item" href="{{ route('unidades.create') }}">Cadastrar Unidade</a></li>
								<li><a class="dropdown-item" href="{{ route('unidades.list') }}">Listar Unidades</a></li>
							</ul>
							</li>
							@else
							<li class="nav-item">
								<a class="nav-link text-white" href="{{ route('agendamentos.index') }}">Meus Agendamentos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-white" aria-current="page" href="{{ route('agendamentos.create') }}">Solicitar Agendamento</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-white" href="{{ route('agendamentos.historico') }}">Histórico</a>
							</li>
							@endif
						@endauth
				</ul>

				<ul class="navbar-nav">
					  @if (Auth::user())
						  <li class="nav-item dropdown float-end">
						  <a class="nav-link" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							  <i class="bi bi-person-circle"></i> {{ Auth::user()->nome }}
						  </a>
						  <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
							  @if (Auth::user()->admin == 0)
							  	<li><a class="dropdown-item" href="{{ route('pacientes.show', Auth::user()->id) }}"><i class="bi bi-clipboard"></i> Meus dados</a></li>
								  <li><hr class="dropdown-divider"></li>
							  @endif
							  <li><a class="dropdown-item" href="{{ route('logout') }}"><i class="bi bi-box-arrow-right"></i> Sair</a></li>
						  </ul>
						  </li>
					  @endif
					  @guest
						  <li class="nav-item dropdown float-end">
							  <a class="nav-link" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
								  <i class="bi bi-person-circle"></i> Entrar
							  </a>
							  <ul class="dropdown-menu dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
								  <li><a class="dropdown-item" href="{{ route('login') }}">Login</a></li>
								  <li><a class="dropdown-item" href="{{ route('site.register') }}">Cadastrar</a></li>
							  </ul>
						  </li>
					  @endguest
				</ul>
			</div>
        </div>
    </div>
  </nav>