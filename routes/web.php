<?php

use App\Http\Controllers\AgendamentoController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\EspecialidadeController;
use App\Http\Controllers\HorarioController;
use App\Http\Controllers\MedicoController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UnidadeController;
use App\Http\Controllers\UserController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix' => 'admin'], function() {
        //horarios
        Route::get('/horarios', [HorarioController::class, 'index'])->name('horarios.list');
        Route::get('/horarios/novo', [HorarioController::class, 'create'])->name('horarios.create');
        Route::post('/horarios/store', [HorarioController::class, 'store'])->name('horarios.store');
        Route::delete('/horarios/{id}', [HorarioController::class, 'destroy'])->name('horarios.delete');
    
        //medicos
        Route::get('/medicos', [MedicoController::class, 'index'])->name('medicos.list');
        Route::get('/medicos/novo', [MedicoController::class, 'create'])->name('medicos.create');
        Route::get('/medicos/editar/{id}', [MedicoController::class, 'editar'])->name('medicos.edit');
        Route::put('/medicos/{id}', [MedicoController::class, 'update'])->name('medicos.update');
        Route::post('/medicos/store', [MedicoController::class, 'store'])->name('medicos.store');
        Route::get('/medicos/{id}', [MedicoController::class, 'show'])->name('medicos.show');
        Route::delete('/medicos/{id}', [MedicoController::class, 'destroy'])->name('medicos.delete');
    
        //unidades
        Route::get('/unidades', [UnidadeController::class, 'index'])->name('unidades.list');
        Route::get('/unidades/novo', [UnidadeController::class, 'create'])->name('unidades.create');
        Route::post('/unidades/store', [UnidadeController::class, 'store'])->name('unidades.store');
        Route::get('/unidades/{id}', [UnidadeController::class, 'show'])->name('unidades.show');

        // pacientes
        Route::get('/pacientes', [UserController::class, 'list'])->name('pacientes.list');
        Route::get('/pacientes/novo', [UserController::class, 'create'])->name('pacientes.create');
        Route::put('/pacientes/update/{id}', [UserController::class, 'update'])->name('pacientes.update');
        Route::get('/pacientes/{id}', [UserController::class, 'show'])->name('pacientes.show');
        Route::post('/pacientes/store', [UserController::class, 'store'])->name('pacientes.store');
        Route::delete('/pacientes/{id}', [UserController::class, 'destroy'])->name('pacientes.delete');

        
        //Route::get('/pacientes/{id}', [UserController::class, 'show'])->name('pacientes.show');

        Route::put('/agendamentos/update/{id}', [AgendamentoController::class, 'update'])->name('agendamentos.update');
        Route::post('/agendamentos/paciente', [AgendamentoController::class, 'getPaciente'])->name('agendamentos.paciente');
        Route::post('/agendamentos/pesquisa', [AgendamentoController::class, 'pesquisar'])->name('agendamentos.search');

        //especialidades
        Route::post('/medicos/nova_especialidade', [EspecialidadeController::class, 'store'])->name('especialidades.create');


    });

    //agendamentos
    Route::get('/', [AgendamentoController::class, 'index'])->name('agendamentos.index');
    Route::get('/agendamentos/historico', [AgendamentoController::class, 'historico'])->name('agendamentos.historico');
    Route::get('/agendamentos/novo/', [AgendamentoController::class, 'create'])->name('agendamentos.create');
    Route::get('/agendamentos/{id}', [AgendamentoController::class, 'show'])->name('agendamentos.show');
    Route::post('/agendamentos/store', [AgendamentoController::class, 'store'])->name('agendamentos.store');
    
    
    // AJAX
    Route::post('/especialidades', [HorarioController::class, 'especialidades'])->name('especialidades');
    Route::post('/horarios', [AgendamentoController::class, 'horarios'])->name('agendamentos.horarios');
    Route::post('/datas', [AgendamentoController::class, 'datas'])->name('agendamentos.datas');
    
});
//site
Route::get('/cadastrar', [SiteController::class, 'register'])->name('site.register');
Route::post('/cadastrar', [RegisteredUserController::class, 'store'])->name('site.user.store');

require __DIR__.'/auth.php';